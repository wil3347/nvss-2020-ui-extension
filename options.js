// Saves options to chrome.storage
function save_options() {
  var options = {
    nostalgic: document.getElementById('nostalgic').checked,
    pack: document.getElementById('pack').value
  }
  
  chrome.storage.local.set(options, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'OK !';
    setTimeout(function() {
      status.textContent = '';
    }, 1500);
  });
}

// Restores select box and checkbox state using the preferences stored in chrome.storage.
function restore_options() {
  var defaultOptions = {
    nostalgic: true,
    pack: 'default'
  }
  
  chrome.storage.local.get(defaultOptions, function(options) {
    document.getElementById('nostalgic').checked = options.nostalgic;
    document.getElementById('pack').value = options.pack;
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);