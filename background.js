let extensionBaseURL;

function setExtensionBaseURL(newPack) {
  console.log("Setting value of extensionBaseURL for pack " + newPack);
  extensionBaseURL = chrome.extension.getURL("img/" + newPack + "/");
}

chrome.storage.local.get({ pack: "default" }, function(options) {
  setExtensionBaseURL(options.pack);
});
  
function redirect(requestDetails) {
  return {
    redirectUrl: requestDetails.url.split("://")[1].replace("loka.zd.fr/nvs/", extensionBaseURL)
  };
}

chrome.webRequest.onBeforeRequest.addListener(
  redirect, 
  {
    urls: [
      "*://loka.zd.fr/nvs/fond_carte/*", 
      "*://loka.zd.fr/nvs/images_perso/*"
    ], 
    types: ["image"]
  },
  ["blocking"]
);

function initDefaultOptions() {
  chrome.storage.local.set({
    nostalgic: true,
    pack: 'default'
  });
}

chrome.runtime.onInstalled.addListener(initDefaultOptions);

chrome.storage.onChanged.addListener(function(changes, namespace) {
  for (var key in changes) {
    var storageChange = changes[key];
    console.log('Storage key "%s" in namespace "%s" changed. Old value was "%s", new value is "%s".', key, namespace, storageChange.oldValue, storageChange.newValue);
    if ("pack" === key) {
      setExtensionBaseURL(storageChange.newValue);
    }
  }
});


chrome.runtime.onMessage.addListener(handleClassementData);

function handleClassementData(message) {
  chrome.storage.local.set({
    persos: message.persos.sort(sortInsensitive)
  });
}

function sortInsensitive(a, b) {
  return a.localeCompare(b, 'en', {'sensitivity': 'base'});
}
