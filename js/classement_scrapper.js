let classementTable = document.querySelector("table");

let persos = [];

for(let row of classementTable.rows) {
    if(row.rowIndex === 0) continue;
    persos.push(getPerso(row));
}

function getPerso(row) {
  let nom = row.cells[1].innerText;
  let mat = row.cells[2].innerText;
  return nom + " [" + mat + "]";
}

console.log("Send Message : persos = " + persos);
chrome.runtime.sendMessage({"persos": persos});
