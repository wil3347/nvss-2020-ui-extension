chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  let gameBody = document.getElementById("gameBody");
  let caracsPersoPv = gameBody.rows[0].cells[0].firstElementChild;
  let caracsPersoReste = caracsPersoPv.nextElementSibling;
  
  caracsPersoPv.id = "caracsPersoPv";
  caracsPersoReste.id="caracsPersoReste";
  
  caracsPersoPv.removeAttribute("style");
  caracsPersoPv.removeAttribute("width");
  
  caracsPersoPv.rows[0].cells[0].className="td";
  caracsPersoPv.rows[0].cells[1].className="td";
  caracsPersoPv.rows[0].cells[1].firstElementChild.id="pvBarTable";
  
  let caracsPersoTables = caracsPersoReste.querySelectorAll("table");
  let caracsPersoLeft = caracsPersoTables[0];
  let caracsPersoCenter = caracsPersoTables[1];
  let caracsPersoRight = caracsPersoTables[2];
  
  caracsPersoLeft.id = "caracsPersoLeft";
  caracsPersoCenter.id = "caracsPersoCenter";
  caracsPersoRight.id = "caracsPersoRight";
  
  caracsPersoReste.removeAttribute("style");
  caracsPersoReste.removeAttribute("width");
  caracsPersoReste.rows[0].removeAttribute("style");
  caracsPersoReste.rows[0].cells[0].removeAttribute("style");
  caracsPersoReste.rows[0].cells[1].removeAttribute("style");
  caracsPersoReste.rows[0].cells[2].removeAttribute("style");

  caracsPersoLeft.removeAttribute("style");
  caracsPersoLeft.removeAttribute("border");
  caracsPersoLeft.removeAttribute("bordercolor");
  
  caracsPersoCenter.removeAttribute("style");
  caracsPersoCenter.removeAttribute("border");
  caracsPersoCenter.removeAttribute("bordercolor");
  
  caracsPersoRight.removeAttribute("style");
  caracsPersoRight.removeAttribute("border");
  caracsPersoRight.removeAttribute("bordercolor");
  
  caracsPersoTables.forEach( (table) => {
    for(let row of table.rows) {
      row.cells[1].className = "center";
    }
  });

});