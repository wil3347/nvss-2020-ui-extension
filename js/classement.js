chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }
  
  document.body.removeAttribute("background");
  document.body.setAttribute("id", "classementBody");

  let classementTable = document.querySelector("#classementBody table");
  classementTable.setAttribute("id", "classementTable");
  
  classementTable.querySelectorAll("img").forEach( (img) => {
    img.remove();
  });

  classementTable.querySelectorAll("a").forEach( (a) => {
    a.setAttribute("target", "_blank");
  });

  });