chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  let infoTable = document.getElementById("infoTable");
  let newInfoTable;
  
  let iconImg = infoTable.rows[0].cells[0].querySelector("img");
  iconImg.removeAttribute("width");
  iconImg.setAttribute("height", "25");
  
  let selectPersoForm = infoTable.querySelector("form[action='jouer.php']");
  selectPersoForm.firstElementChild.remove();
  
  let gradeLink = infoTable.querySelectorAll("a[href='grades.php']")[1];
  let gradeImg = gradeLink.querySelector("img");
  gradeImg.removeAttribute("width");
  gradeImg.setAttribute("height", "25");
  let gradeLinkImg = gradeLink.cloneNode(false);
  gradeLinkImg.appendChild(gradeImg);
  
  let bataillonLink = infoTable.querySelector("a[href^='bataillon.php']");
  let chefText = infoTable.rows[1].cells[0].innerText.split(' : ')[1];
  let compagnieLink = infoTable.querySelector("a[href='compagnie.php']");
  
  injectNewInfoTable();
  
  function injectNewInfoTable() {
    newInfoTable = document.createElement("table");
    newInfoTable.id = "infoTable";
    
    // Row 0
    let row0 = newInfoTable.insertRow();
    row0.insertCell().appendChild(iconImg);
    row0.insertCell().appendChild(text("Nom"));
    row0.insertCell().appendChild(selectPersoForm);
    row0.insertCell().appendChild(text("Grade"));
    row0.insertCell().appendChild(gradeLinkImg);
    row0.insertCell().appendChild(gradeLink);
    
    let row1 = newInfoTable.insertRow();
    colspan2(row1.insertCell(), text("Bataillon"));
    row1.insertCell().appendChild(bataillonLink);
    row1.insertCell().appendChild(text("Chef"));
    colspan2(row1.insertCell(), createSpan("white", chefText));
    
    let row2 = newInfoTable.insertRow();
    colspan2(row2.insertCell(), text("Cie"));
    row2.insertCell().appendChild(compagnieLink);
    row2.insertCell().appendChild(text("Section"));
    colspan2(row2.insertCell(), text(""));
    
    infoTable.removeAttribute("id");
    infoTable.parentNode.insertBefore(newInfoTable, infoTable);
  }
  
  injectNewInfoTour();
  
  function injectNewInfoTour() {
    let newInfoTour = document.createElement("table");
    newInfoTour.id = "infoTour";
    let row0 = newInfoTour.insertRow();
    
    let uniteImg = document.createElement("img");
    uniteImg.src=findUniteImgSrc();
    
    let cellImg = row0.insertCell();
    cellImg.setAttribute("rowspan","3");
    cellImg.appendChild(uniteImg);
    row0.insertCell().appendChild(text("Heure Serveur :\u00A0"));
    row0.insertCell().appendChild(document.getElementById("tp1"));
    
    let row1 = newInfoTour.insertRow();
    row1.insertCell().appendChild(text("Prochain tour :\u00A0"));
    let prochainTour = infoTable.rows[1].cells[3].innerText.split(' : ')[1];
    row1.insertCell().appendChild(createSpan("white", prochainTour));
    
    let refreshLink = document.createElement("a");
    refreshLink.href="jouer.php";
    refreshLink.target="_self";
    refreshLink.text="Rafraîchir la page";
    colspan2(newInfoTour.insertRow().insertCell(), refreshLink);

    insertAfter(newInfoTour, newInfoTable);
    infoTable.remove();
  }
  
  function text(text){
    return document.createTextNode(text);
  }
  
  function colspan2(cell, node) {
    cell.setAttribute("colspan", "2");
    cell.appendChild(node);
  }
  
  function createSpan(className, text) {
      let span = document.createElement("span");
      span.className = className;
      span.innerText = text;
      return span;
  }
  
  function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
  
  function findUniteImgSrc() {
    if(iconImg.src.includes("infanterie")) { return "../images_perso/Infanterie.jpg"; }
    if(iconImg.src.includes("cavalerie")) { return "../images_perso/Cavalerie.jpg"; }
    if(iconImg.src.includes("artillerie")) { return "../images_perso/Artillerie.jpg"; }
    
    return iconImg.src;
  }
  
});