[
  chrome.extension.getURL("vendor/sceditor-2.1.3/minified/themes/default.min.css"),
  chrome.extension.getURL("vendor/accessible-autocomplete-2.0.2/accessible-autocomplete.css")
].forEach(function(src) {
  var link = document.createElement('link');
  link.rel = "stylesheet";
  link.href = src;
  link.async = false;
  document.head.appendChild(link);
});

[
  chrome.extension.getURL("vendor/sceditor-2.1.3/minified/sceditor.min.js"),
  chrome.extension.getURL("vendor/sceditor-2.1.3/minified/formats/bbcode.js"),
  chrome.extension.getURL("vendor/sceditor-2.1.3/languages/fr.js")
].forEach(function(src) {
  var script = document.createElement('script');
  script.src = src;
  script.async = false;
  document.head.appendChild(script);
});

[
  chrome.extension.getURL("js/nouveau_message_init.js")
].forEach(function(src) {
  var script = document.createElement('script');
  script.src = src;
  script.setAttribute("data-baseurl", chrome.extension.getURL("vendor/sceditor-2.1.3/minified/themes/content/default.min.css"));
  script.async = false;
  document.head.appendChild(script);
});

// Autocomplete

let destinataireInput = document.querySelector("#destinataireInput");
let destinataireInputTd = destinataireInput.parentElement;
destinataireInputTd.setAttribute("colspan","2");
destinataireInputTd.parentElement.insertBefore(createAutocompleteTd(),destinataireInputTd);

function createAutocompleteTd() {
  let autocompleteTd = document.createElement("td");
  autocompleteTd.setAttribute("width", "15%");
  autocompleteTd.appendChild(createAutocompleteLabel());
  autocompleteTd.appendChild(createAutocompleteContainer());
  return autocompleteTd;
}

function createAutocompleteLabel() {
  let label = document.createElement("label");
  label.setAttribute("id", "autocompleteLabel");
  label.setAttribute("for", "autocompleteInput");
  label.setAttribute("style", "display:none");
  return label;
}

function createAutocompleteContainer() {
  let div = document.createElement("div");
  div.setAttribute("id", "autocompleteContainer");
  return div;
}

chrome.storage.local.get({ persos: [] }, initAutocomplete);
let autocompleteContainer = document.querySelector("#autocompleteContainer");

function initAutocomplete(data) {
  accessibleAutocomplete({
    element: autocompleteContainer,
    id: 'autocompleteInput',
    source: data.persos,
    autoselect: false,
    confirmOnBlur: false,
    defaultValue: "",
    placeholder: 'Rechercher...',
    minLength: 2,
    displayMenu: 'overlay',
    tNoResults: () => 'Aucun résultat',
    onConfirm: addToRecipients
  });
}

function addToRecipients(val) {
  if(val !== undefined) {
    if(destinataireInput.value !== undefined && destinataireInput.value.length != 0) {
      destinataireInput.value += ';';
    }
    destinataireInput.value += val.split('[')[1].split(']')[0];
  }
}
