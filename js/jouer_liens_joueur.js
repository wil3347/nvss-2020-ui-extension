chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  let hrpMenu = document.getElementById("hrpMenu");
  let infoTable = document.getElementById("infoTable");
  
  // Ajout des liens joueur en haut
  let liensJoueur = document.createElement("div");
  liensJoueur.id = "liensJoueur";
  document.body.insertBefore(liensJoueur, hrpMenu);
   
  function injectJoueurAnchorLink(href, text, target = "_blank", parentElement = liensJoueur) {
    let a = document.createElement("a");
    a.href = href;
    a.text = text;
    a.target = target;
    parentElement.appendChild(a);
  }
  
  function injectJoueurAchorLinkSeparator() {
    liensJoueur.appendChild(document.createTextNode("\u00A0\u00A0-\u00A0\u00A0"));  
  }
  
  injectJoueurAnchorLink("compte.php", "Mon compte");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("../", "Accueil");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("../regles/regles.php", "Les règles");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("classement.php", "Les classements");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("compagnie.php?voir_compagnie=ok", "Les compagnies");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("http://nordvssud-creation.forumactif.com/", "Le Forum");
  injectJoueurAchorLinkSeparator();
  injectJoueurAnchorLink("../logout.php", "Déconnexion");
  
  // Deplacement des heures (info tour)
  let heureServeurCell = hrpMenu.rows[0].cells[0];
  let prochainTourCell = hrpMenu.rows[1].cells[0];
  
  injectJoueurAnchorLink("jouer.php", " - Rafraîchir la page", "_self", heureServeurCell);

  infoTable.rows[0].appendChild(heureServeurCell);
  infoTable.rows[1].appendChild(prochainTourCell);
  
  hrpMenu.remove();
  
});